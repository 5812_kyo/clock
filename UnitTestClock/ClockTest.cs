﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;
using System.Drawing;

using Clock3.Interface;
using Clock3.Model;
using NUnit.Framework;
using Clock3;

namespace UnitTestClock
{
    [TestFixture]
    public class ClockTest
    {
       
        [TestCase( 100,  10,  10,  25,  40,  10,  4,  4,  5, TestName="Тестируем минимальные значения параметров")]
        [TestCase(200, 20, 50, 70, 100, 20, 12, 12, 10, TestName="Тестируем максимальные значения параметров")]
        [TestCase(150, 15, 15, 45, 65, 15, 8, 8, 7, TestName="Тестируем среднее значение параметров")]
        [TestCase(100, 20, 25, 35, 50, 20, 12, 12, 10, TestName="Тестируем параметры входящие в диапозон значений")]
        [TestCase(200, 10, 20, 50, 80, 10, 4, 4, 5, TestName="Тестируем параметры входящие в диапозон значений.")]

        [TestCase(300, 10, 10, 25, 40, 10, 4, 4, 5, TestName = "Тестируем значение параметра, выходящий за границы ширины каркаса", ExpectedException = typeof(Exception))]
        [TestCase(100, 30, 10, 25, 40, 10, 4, 4, 5, TestName="Тестируем значение параметра, выходящий за границы высоты каркаса", ExpectedException = typeof(Exception))]
        [TestCase(100, 10, 30, 25, 40, 10, 4, 4, 5, TestName = "Тестируем значение параметра, выходящий за границы ширины стрелок", ExpectedException = typeof(Exception))]
        [TestCase(100, 10, 10, 45, 40, 10, 4, 4, 5, TestName = "Тестируем значение параметра, выходящий за границы длины маленькой стрелки", ExpectedException = typeof(Exception))]
        [TestCase(100, 10, 10, 25, 65, 10, 4, 4, 5, TestName = "Тестируем значение параметра, выходящий за границы длины большой стрелки", ExpectedException = typeof(Exception))]
        [TestCase(100, 10, 10, 25, 40, 35, 4, 4, 5, TestName = "Тестируем значение параметра, выходящий за границы длины кнопки", ExpectedException = typeof(Exception))]
        [TestCase(100, 10, 10, 25, 40, 20, 56, 4, 5, TestName = "Тестируем значение параметра, выходящий за границы количества зубцов на кнопке", ExpectedException = typeof(Exception))]
        [TestCase(100, 10, 10, 25, 40, 20, 12, 78, 5, TestName = "Тестируем значение параметра, выходящий за границы количества отверстий на крышке", ExpectedException = typeof(Exception))]
        [TestCase(100, 10, 10, 25, 40, 20, 12, 12, 58, TestName = "Тестируем значение параметра, выходящий за границы длины цепочки", ExpectedException = typeof(Exception))]

        [TestCase(-100, 10, 10, 25, 40, -10, -4, 4, 5, TestName="Проверка на отрицательные значения параметров", ExpectedException = typeof(Exception))]

        public void test(double WidthCarcass, double HeightCarcass, double WidthPointers, double LengthSmallPointer,
            double LengthBigPointer, double LengthButton, double NumberRotch, double NumberHoles, double LengthChain)
        {
            var parameters = new List<double>()
            {
            WidthCarcass,
            HeightCarcass,
            WidthPointers,
            LengthSmallPointer,
            LengthBigPointer,
            LengthButton,
            NumberRotch,
            NumberHoles,
            LengthChain};

            ClockFigureType figureType = ClockFigureType.Circle;
            Parameters param = new Parameters(figureType);
            param.InputParameters(parameters);            
        }
    }
}
