﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clock3.Interface;

using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;

namespace Clock3.Model
{
    class Cap : IPart
    {
        /// <summary>
        /// Построение крышки часов
        /// </summary>       
        public void CreatePart(Parameters parameters, Database database, 
            Transaction trans, ClockFigureType FigureType)
        {
            // Открываем Block table для чтения
            BlockTable BlockTable;
            BlockTable = trans.GetObject(database.BlockTableId,
            OpenMode.ForRead) as BlockTable;

            // Открываем the Block table для записи
            BlockTableRecord BlockTableRec;
            BlockTableRec = trans.GetObject(BlockTable[BlockTableRecord.ModelSpace],
            OpenMode.ForWrite) as BlockTableRecord;

            // Создаем цилиндр
            Solid3d Cap = new Solid3d();
            Cap.SetDatabaseDefaults();
            Cap.CreateFrustum(parameters.HeightCarcass, parameters.WidthCarcass,
                parameters.WidthCarcass, parameters.WidthCarcass);
            Cap.Color = Color.FromNames(Properties.Resources.ClockColor, Properties.Resources.Library2);

            // Позиция центра 3D тела
            Cap.TransformBy(Matrix3d.Displacement(new Point3d(0, parameters.WidthCarcass + parameters.HeightCarcass / 2,
                parameters.WidthCarcass + parameters.HeightCarcass / 2) - Point3d.Origin));
            // Вращение
            Vector3d vRot = new Point3d(0, 0, 0).GetVectorTo(new Point3d(1, 0, 0));
            Cap.TransformBy(Matrix3d.Rotation(Math.PI / 2, vRot, 
                new Point3d(0, parameters.WidthCarcass + parameters.HeightCarcass / 2, 
                    parameters.WidthCarcass + parameters.HeightCarcass / 2)));

            // Добавляем новый объект в block table и транзакцию
            BlockTableRec.AppendEntity(Cap);
            trans.AddNewlyCreatedDBObject(Cap, true);


            // Скругление крышки....................................................................

            ObjectId[] ids = new ObjectId[] { Cap.ObjectId };
            SubentityId subentId = new SubentityId(
            SubentityType.Null, IntPtr.Zero);
            FullSubentityPath path = new FullSubentityPath(
            ids, subentId);
            List<SubentityId> subentIds = new List<SubentityId>();
            DoubleCollection radii = new DoubleCollection();
            DoubleCollection startSetback = new DoubleCollection();
            DoubleCollection endSetback = new DoubleCollection();
            using (Autodesk.AutoCAD.BoundaryRepresentation.Brep brep =
            new Autodesk.AutoCAD.BoundaryRepresentation.Brep(path))
            {
                foreach (Autodesk.AutoCAD.BoundaryRepresentation.Edge
                    edge in brep.Edges)
                {
                    subentIds.Add(edge.SubentityPath.SubentId);
                    radii.Add(parameters.HeightCarcass/3);

                    // Определяем на сколько будем скруглять
                    startSetback.Add(parameters.HeightCarcass*0.1);
                    endSetback.Add(parameters.HeightCarcass * 0.1);
                }
            }
            Cap.FilletEdges(
            subentIds.ToArray(),
            radii,
            startSetback,
            endSetback);





            // Создаем углубление в крышке..............................................................

            Solid3d Cylinder = new Solid3d();
            Cylinder.SetDatabaseDefaults();
            var height = parameters.HeightCarcass / 2;
            var radius = parameters.WidthCarcass * 0.85;
            Cylinder.CreateFrustum(height, radius, radius, radius);
            Cylinder.Color = Color.FromNames(Properties.Resources.ClockColor2, Properties.Resources.Library2);

            // Позиция центра 3D тела
            Cylinder.TransformBy(Matrix3d.Displacement(new Point3d(0, parameters.WidthCarcass,
                parameters.WidthCarcass + parameters.HeightCarcass / 2) - Point3d.Origin));
            // Вращение
            Cylinder.TransformBy(Matrix3d.Rotation(Math.PI / 2, vRot, new Point3d(0, parameters.WidthCarcass,
                parameters.WidthCarcass + parameters.HeightCarcass / 2)));

         
            
            Cap.BooleanOperation(BooleanOperationType.BoolSubtract, Cylinder);


            double n = parameters.NumberHoles;
            double coordX = 0;
            double coordY = parameters.WidthCarcass + parameters.HeightCarcass/2;
            double coordZ = 0;
            double angleHoleslements = 360;
            double angleHolesStep = 360 / n;
            var insideCap = parameters.WidthCarcass * 0.6;

            for (int i = 0; i < n; i++)
            {
                coordX = (insideCap) * Math.Cos(angleHoleslements * Math.PI / 180);
                coordZ = (insideCap) * Math.Sin(angleHoleslements * Math.PI / 180);
                coordZ += parameters.WidthCarcass;
                BuildHoles(database, trans, parameters, coordX, coordY, coordZ, Cap);

                angleHoleslements -= angleHolesStep;
            }

            // Добавляем новый объект в block table и транзакцию
            BlockTableRec.AppendEntity(Cylinder);
            trans.AddNewlyCreatedDBObject(Cylinder, true);


          }

            /// <summary>
            /// Вырезаем отверстия в крышке
            /// </summary>        
            private void BuildHoles(Database database, Transaction trans, Parameters parameters,
              double coordinateX, double coordinateY, double coordinateZ, Solid3d Cylinder)
          {

            // Открываем Block table для чтения
            BlockTable BlockTable = trans.GetObject(database.BlockTableId, OpenMode.ForRead) as BlockTable;

            // Открываем the Block table для записи
            BlockTableRecord BlockTableRec = trans.GetObject(BlockTable[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;

            // Создаем  цилиндр
            Solid3d Holes = new Solid3d();
            Holes.SetDatabaseDefaults();
            var radius = parameters.WidthCarcass / 10;
            Holes.CreateFrustum(parameters.HeightCarcass, radius, radius, radius);
            Holes.Color = Color.FromNames(Properties.Resources.ClockColor2, Properties.Resources.Library2);
            

            // Позиция центра 3D тела 
            Holes.TransformBy(Matrix3d.Displacement(new Point3d(coordinateX, coordinateY, 
                coordinateZ) - Point3d.Origin));
            // Вращение
            Vector3d vRot = new Point3d(0, 0, 0).GetVectorTo(new Point3d(1, 0, 0));
            Holes.TransformBy(Matrix3d.Rotation(Math.PI/2, vRot, new Point3d(coordinateX, coordinateY, coordinateZ)));

            // Добавляем новый объект в block table и транзакцию
            BlockTableRec.AppendEntity(Holes);
            trans.AddNewlyCreatedDBObject(Holes, true);

              
            Cylinder.BooleanOperation(BooleanOperationType.BoolSubtract, Holes);
            
          }
        }
    }
