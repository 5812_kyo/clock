﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clock3.Interface;

using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;

namespace Clock3.Model
{
    class ClockFigures : IPart
    {
        /// <summary>
        /// Создание фигур циферблата
        /// </summary>      
        public void CreatePart(Parameters parameters, Database database,
            Transaction trans, ClockFigureType FigureType)
        {
            // Open the Block table record for read
            BlockTable BlockTable;
            BlockTable = trans.GetObject(database.BlockTableId,
            OpenMode.ForRead) as BlockTable;

            // Open the Block table record Model space for write
            BlockTableRecord BlockTableRec;
            BlockTableRec = trans.GetObject(BlockTable[BlockTableRecord.ModelSpace],
            OpenMode.ForWrite) as BlockTableRecord;
            var clockFace = parameters.WidthCarcass * 0.7;
            var indent = 0.3 * parameters.HeightCarcass; 
            int n = 12;
            double coordX = 0;
            double coordY = 0;
            double coordZ = indent;
            double angleFiguresElements = 360;
            double angleFiguresStep = 30;

            for (int i = 0; i < n; i++)
            {
                coordX = (clockFace) * Math.Cos(angleFiguresElements * Math.PI / 180);
                coordY = (clockFace) * Math.Sin(angleFiguresElements * Math.PI / 180);

                if (FigureType == ClockFigureType.Square)
                {
                    BuildSquare(database, trans, parameters, coordX, coordY, coordZ);
                }
                else
                    BuildCircle(database, trans, parameters, coordX, coordY, coordZ);             
                                          
                
                angleFiguresElements += angleFiguresStep;
            }
        }

        /// <summary>
        /// Построение квадратных фигур циферблата
        /// </summary>                
        private void BuildSquare(Database database, Transaction trans, Parameters parameters,
                  double coordinateX,double coordinateY, double coordinateZ)
        {

            // Open the Block table record for read
            BlockTable BlockTable;
            BlockTable = trans.GetObject(database.BlockTableId,
            OpenMode.ForRead) as BlockTable;

            // Open the Block table record Model space for write
            BlockTableRecord BlockTableRec;
            BlockTableRec = trans.GetObject(BlockTable[BlockTableRecord.ModelSpace],
            OpenMode.ForWrite) as BlockTableRecord;

            // Create a 3D solid box
            Solid3d Square = new Solid3d();
            Square.SetDatabaseDefaults();
            var length = parameters.WidthCarcass / 10;
            var width = parameters.WidthCarcass / 10;
            var height = parameters.WidthPointers / 6;
            Square.CreateBox(width, length, height);
            Square.Color = Color.FromNames(Properties.Resources.FiguresColor, Properties.Resources.Library);

            // Position the center of the 3D solid at (5,5,0) 
            Square.TransformBy(Matrix3d.Displacement(new Point3d(coordinateX, coordinateY,
                coordinateZ) - Point3d.Origin));


            // Add the new object to the block table record and the transaction
            BlockTableRec.AppendEntity(Square);
            trans.AddNewlyCreatedDBObject(Square, true);         
        }

        /// <summary>
        /// Построение круглых фигур циферблата
        /// </summary>       
        private void BuildCircle(Database database, Transaction trans, Parameters parameters,
                double coordinateX,double coordinateY, double coordinateZ)
       {

            // Open the Block table record for read
            BlockTable BlockTable;
            BlockTable = trans.GetObject(database.BlockTableId,
            OpenMode.ForRead) as BlockTable;
            
            // Open the Block table record Model space for write
            BlockTableRecord BlockTableRec;
            BlockTableRec = trans.GetObject(BlockTable[BlockTableRecord.ModelSpace],
            OpenMode.ForWrite) as BlockTableRecord;
            
            // Create a 3D solid box
            Solid3d Circle = new Solid3d();
            Circle.SetDatabaseDefaults();
            var radius = parameters.WidthCarcass / 20;
            var height = parameters.WidthPointers / 6;
            Circle.CreateFrustum(height, radius, radius, radius);
            Circle.Color = Color.FromNames(Properties.Resources.FiguresColor, Properties.Resources.Library);

            // Position the center of the 3D solid at (5,5,0) 
            Circle.TransformBy(Matrix3d.Displacement(new Point3d(coordinateX, coordinateY, 
               coordinateZ) - Point3d.Origin));


            // Add the new object to the block table record and the transaction
            BlockTableRec.AppendEntity(Circle);
            trans.AddNewlyCreatedDBObject(Circle, true);  
       }
    }
}
