﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clock3.Interface;

using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;


namespace Clock3.Model
{
    class Pointers : IPart
    {
      ///<summary>
     ///Построение стрелок
     ///</summary>

        public void CreatePart(Parameters parameters, Database database, 
            Transaction trans, ClockFigureType FigureType)
        {
            // Open the Block table record for read
            BlockTable BlockTable;
            BlockTable = trans.GetObject(database.BlockTableId,
            OpenMode.ForRead) as BlockTable;

            // Open the Block table record Model space for write
            BlockTableRecord BlockTableRec;
            BlockTableRec = trans.GetObject(BlockTable[BlockTableRecord.ModelSpace],
            OpenMode.ForWrite) as BlockTableRecord;

            //Триб центрального колеса................................................

            // Create a 3D solid cylinder
            // 3D solids are created at (0,0,0) so there is no need to move it
            Solid3d Tribes = new Solid3d();
            Tribes.SetDatabaseDefaults();
            var height = parameters.WidthPointers / 6;
            var radius = parameters.WidthPointers / 2;
            Tribes.CreateFrustum(height, radius, radius, radius);
            Tribes.Color = Color.FromNames(Properties.Resources.FiguresColor, Properties.Resources.Library);

            // Position the center of the 3D solid at (5,5,0) 
            Tribes.TransformBy(Matrix3d.Displacement(new Point3d(0, 0,
                0.3 * parameters.HeightCarcass) - Point3d.Origin));

            // Add the new object to the block table record and the transaction
            BlockTableRec.AppendEntity(Tribes);
            trans.AddNewlyCreatedDBObject(Tribes, true);

            BuildPointers(database, trans, parameters, "big");
            BuildPointers(database, trans, parameters, "small");
        }

        /// <summary>
        /// Построение маленькой и большой стрелок
        /// </summary>        
        private void BuildPointers(Database database, Transaction trans, Parameters parameters,string Pointers)
        {
            double width = 0;
            double length = 0;
            double height = 0;
            double coordX = 0;
            double coordY = 0;
            double coordZ = 0;

            if (Pointers == "big")
            {
                     width = parameters.WidthPointers;
                     length = parameters.LengthBigPointer;
                     height = parameters.WidthPointers / 6;
                     coordX = 0;
                     coordY = -parameters.WidthPointers/2 - parameters.LengthBigPointer / 2;
                     coordZ = 0.3 * parameters.HeightCarcass;
            }
            else
            {
             
                     width = parameters.WidthPointers;
                     length = parameters.LengthSmallPointer;
                     height = parameters.WidthPointers / 6;
                     coordX = 0;
                     coordY = parameters.WidthPointers/2 + parameters.LengthSmallPointer / 2;
                     coordZ = 0.3 * parameters.HeightCarcass;

            }



            // Open the Block table record for read
            BlockTable BlockTable;
            BlockTable = trans.GetObject(database.BlockTableId,
            OpenMode.ForRead) as BlockTable;

            // Open the Block table record Model space for write
            BlockTableRecord BlockTableRec;
            BlockTableRec = trans.GetObject(BlockTable[BlockTableRecord.ModelSpace],
            OpenMode.ForWrite) as BlockTableRecord;


            //Create a 3D solid box
            Solid3d Pointer = new Solid3d();
            Pointer.SetDatabaseDefaults();
            Pointer.CreateBox(width, length, height);
            Pointer.Color = Color.FromNames(Properties.Resources.FiguresColor, Properties.Resources.Library);

            // Position the center of the 3D solid at (5,5,0) 
            Pointer.TransformBy(Matrix3d.Displacement(new Point3d(coordX, coordY, coordZ) - Point3d.Origin));

            // Add the new object to the block table record and the transaction
            BlockTableRec.AppendEntity(Pointer);
            trans.AddNewlyCreatedDBObject(Pointer, true);
        }
    }
}
