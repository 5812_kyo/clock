﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clock3.Interface;

using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;

namespace Clock3.Model
{
    class Pressbutton : IPart
    {
        /// <summary>
        /// Построение кнопки часов
        /// </summary>        
        public void CreatePart(Parameters parameters, Database database,
            Transaction trans, ClockFigureType FigureType)
        {
            // Open the Block table record for read
            BlockTable BlockTable;
            BlockTable = trans.GetObject(database.BlockTableId,
            OpenMode.ForRead) as BlockTable;

            // Open the Block table record Model space for write
            BlockTableRecord BlockTableRec;
            BlockTableRec = trans.GetObject(BlockTable[BlockTableRecord.ModelSpace],
            OpenMode.ForWrite) as BlockTableRecord;

            // Create a 3D solid cylinder
            // 3D solids are created at (0,0,0) so there is no need to move it
            Solid3d button = new Solid3d();
            button.SetDatabaseDefaults();
            var radius = parameters.HeightCarcass / 2;
            button.CreateFrustum(parameters.LengthButton, radius, radius, radius);
            button.Color = Color.FromNames(Properties.Resources.ClockColor, Properties.Resources.Library2);


            // Position the center of the 3D solid at (5,5,0) 
            button.TransformBy(Matrix3d.Displacement(new Point3d(0, -parameters.WidthCarcass 
                - parameters.LengthButton/2, 0) - Point3d.Origin));
            //Rotate
            Vector3d vRot = new Point3d(0, 0, 0).GetVectorTo(new Point3d(1, 0, 0));
            button.TransformBy(Matrix3d.Rotation(Math.PI/2, vRot, new Point3d(0, -parameters.WidthCarcass
                - parameters.LengthButton / 2, 0)));

            // Add the new object to the block table record and the transaction
            BlockTableRec.AppendEntity(button);
            trans.AddNewlyCreatedDBObject(button, true);

            var widthbutton = parameters.HeightCarcass / 2;
            double n = parameters.NumberRotch;
            double coordX = 0;
            double coordY = -parameters.WidthCarcass - parameters.LengthButton / 2;
            double coordZ = 0;
            double angleRotchButton = 0;
            double angleRotchStep = 360 / (n);

            for (int i = 0; i < n; i++)
            {
                coordX = (widthbutton) * Math.Cos(angleRotchButton * Math.PI / 180);
                coordZ = (widthbutton) * Math.Sin(angleRotchButton * Math.PI / 180);

                BuildRotch(database, trans, parameters, coordX, coordY, coordZ);
                
                angleRotchButton += angleRotchStep;
            }


        }

        /// <summary>
        /// Построение зубцов на кнопке
        /// </summary>       
        private void BuildRotch(Database database, Transaction trans, Parameters parameters,
            double coordinateX, double coordinateY, double coordinateZ)
        {

            // Open the Block table record for read
            BlockTable BlockTable;
            BlockTable = trans.GetObject(database.BlockTableId,
            OpenMode.ForRead) as BlockTable;

            // Open the Block table record Model space for write
            BlockTableRecord BlockTableRec;
            BlockTableRec = trans.GetObject(BlockTable[BlockTableRecord.ModelSpace],
            OpenMode.ForWrite) as BlockTableRecord;

            // Create a 3D solid box
            Solid3d Rotch = new Solid3d();
            Rotch.SetDatabaseDefaults();
            Rotch.CreateBox((parameters.HeightCarcass / 2)/parameters.NumberRotch,
                parameters.LengthButton, parameters.HeightCarcass/4);
            Rotch.Color = Color.FromNames(Properties.Resources.ClockColor2, Properties.Resources.Library2);

            // Position the center of the 3D solid at (5,5,0) 
            Rotch.TransformBy(Matrix3d.Displacement(new Point3d(coordinateX, coordinateY, 
                coordinateZ) - Point3d.Origin));


            // Add the new object to the block table record and the transaction
            BlockTableRec.AppendEntity(Rotch);
            trans.AddNewlyCreatedDBObject(Rotch, true);       
        }

    }   
}
