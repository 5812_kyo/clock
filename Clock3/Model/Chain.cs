﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clock3.Interface;

using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;

namespace Clock3.Model
{
    class Chain : IPart
    {
        /// <summary>
        /// Создаем цепочку часов
        /// </summary>       
        public void CreatePart(Parameters parameters, Database database, 
            Transaction trans, ClockFigureType FigureType)
        {
            // Открываем Block table для чтения
            BlockTable BlockTable;
            BlockTable = trans.GetObject(database.BlockTableId,
            OpenMode.ForRead) as BlockTable;

            // Открываем the Block table для записи
            BlockTableRecord BlockTableRec;
            BlockTableRec = trans.GetObject(BlockTable[BlockTableRecord.ModelSpace],
            OpenMode.ForWrite) as BlockTableRecord;

            for (int i = 0; i < parameters.LengthChain; i++)
            {
                // Создаем тор
                Solid3d tor = new Solid3d();
                tor.SetDatabaseDefaults();
                tor.CreateTorus(parameters.HeightCarcass, 1);               
                tor.Color = Color.FromNames(Properties.Resources.ClockColor2, Properties.Resources.Library2);
                // Позиция центра 3D тела
                tor.TransformBy(Matrix3d.Displacement(new Point3d(0, (-parameters.WidthCarcass - parameters.LengthButton)
                    - (parameters.LengthChain*(2.0 * parameters.HeightCarcass  - 4.0 * 1.0)), 0) - Point3d.Origin));

                tor.TransformBy(Matrix3d.Displacement(new Point3d(0, Convert.ToDouble(i) 
                    * (2.0 * parameters.HeightCarcass - 2.0 * 1.0), 0) - Point3d.Origin));
                // Вращение
                Vector3d vRot = new Point3d(0, 0, 0).GetVectorTo(new Point3d(0, 1, 0));
                if (i % 2 == 0)
                {
                    vRot = new Point3d(0, 0, 0).GetVectorTo(new Point3d(0, 0, 0));
                }
                tor.TransformBy(Matrix3d.Rotation(Math.PI/2, vRot, new Point3d(0, (Convert.ToDouble(i) 
                    * ((2.0 * parameters.HeightCarcass + 1) - 4.0 * 1.0)), 0)));

                // Добавляем новый объект в block table и транзакцию
                BlockTableRec.AppendEntity(tor);
                trans.AddNewlyCreatedDBObject(tor, true);
            }
        }
    }
}
