﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clock3.Interface;

using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;

namespace Clock3.Model
{
    class Carcass : IPart
    {
        /// <summary>
        /// Построение каркаса часов
        /// </summary>
        public void CreatePart(Parameters parameters, Database database, 
            Transaction trans, ClockFigureType FigureType)
        {
                // Открываем Block table для чтения
                BlockTable BlockTable;
                BlockTable = trans.GetObject(database.BlockTableId,
                OpenMode.ForRead) as BlockTable;

                // Открываем the Block table для записи
                BlockTableRecord BlockTableRec;
                BlockTableRec = trans.GetObject(BlockTable[BlockTableRecord.ModelSpace],
                OpenMode.ForWrite) as BlockTableRecord;


                // Создаем цилиндр
                Solid3d Carcass = new Solid3d();
                Carcass.SetDatabaseDefaults();
                Carcass.CreateFrustum(parameters.HeightCarcass, parameters.WidthCarcass,
                    parameters.WidthCarcass, parameters.WidthCarcass);
                Carcass.Color = Color.FromNames(Properties.Resources.ClockColor, Properties.Resources.Library2);


                // Добавляем новый объект в block table и транзакцию
                BlockTableRec.AppendEntity(Carcass);
                trans.AddNewlyCreatedDBObject(Carcass, true);


                // Скругление каркаса..............................................................................

                ObjectId[] ids = new ObjectId[] { Carcass.ObjectId };
                SubentityId subentId = new SubentityId(
                SubentityType.Null, IntPtr.Zero);
                FullSubentityPath path = new FullSubentityPath(
                ids, subentId);
                List<SubentityId> subentIds = new List<SubentityId>();
                DoubleCollection radii = new DoubleCollection();
                DoubleCollection startSetback = new DoubleCollection();
                DoubleCollection endSetback = new DoubleCollection();
                using (Autodesk.AutoCAD.BoundaryRepresentation.Brep brep =
                new Autodesk.AutoCAD.BoundaryRepresentation.Brep(path))
                {
                    foreach (Autodesk.AutoCAD.BoundaryRepresentation.Edge
                        edge in brep.Edges)
                    {
                        subentIds.Add(edge.SubentityPath.SubentId);
                        radii.Add(parameters.HeightCarcass / 3);


                        // Определяем на сколько будем скруглять
                        startSetback.Add(parameters.HeightCarcass*0.1);
                        endSetback.Add(parameters.HeightCarcass*0.1);
                    }
                }
                Carcass.FilletEdges(
                subentIds.ToArray(),
                radii,
                startSetback,
                endSetback);

                // Создаем углубление в каркасе.............................................................................

                Solid3d Cylinder = new Solid3d();
                Cylinder.SetDatabaseDefaults();
                var radius = parameters.WidthCarcass * 0.85;
                var height = parameters.HeightCarcass / 2;
                Cylinder.CreateFrustum(height, radius, radius, radius);
                Cylinder.Color = Color.FromNames(Properties.Resources.ClockColor2, Properties.Resources.Library2);
                var indent = 0.3 * parameters.HeightCarcass;
                // Позиция центра 3D тела 
                Cylinder.TransformBy(Matrix3d.Displacement(new Point3d(0, 0, indent) - Point3d.Origin));

                // Добавляем новый объект в block table и транзакцию
                BlockTableRec.AppendEntity(Cylinder);
                trans.AddNewlyCreatedDBObject(Cylinder, true);

                Carcass.BooleanOperation(BooleanOperationType.BoolSubtract, Cylinder);             
            
        }
    }
}
