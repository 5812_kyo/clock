﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clock3.Interface;

using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;

namespace Clock3.Model
{
    class Mount : IPart 
    {
        /// <summary>
        /// Создание крепления часов
        /// </summary>       
        public void CreatePart(Parameters parameters, Database database, 
            Transaction trans, ClockFigureType FigureType)
        {

            // Open the Block table record for read
            BlockTable BlockTable;
            BlockTable = trans.GetObject(database.BlockTableId,
            OpenMode.ForRead) as BlockTable;

            // Open the Block table record Model space for write
            BlockTableRecord BlockTableRec;
            BlockTableRec = trans.GetObject(BlockTable[BlockTableRecord.ModelSpace],
            OpenMode.ForWrite) as BlockTableRecord;

            // Create a 3D solid cylinder
            // 3D solids are created at (0,0,0) so there is no need to move it
            Solid3d Mount = new Solid3d();
            Mount.SetDatabaseDefaults();
            var radius = 0.5 * parameters.HeightCarcass;
            var height = parameters.HeightCarcass * 3;
            Mount.CreateFrustum(height, radius, radius, radius);
            Mount.Color = Color.FromNames(Properties.Resources.ClockColor, Properties.Resources.Library2);

            // Position the center of the 3D solid at (5,5,0) 
            Mount.TransformBy(Matrix3d.Displacement(new Point3d(0, parameters.WidthCarcass,
                parameters.HeightCarcass/2) - Point3d.Origin));
            //Rotate
            Vector3d vRot = new Point3d(0, 0, 0).GetVectorTo(new Point3d(0, 1, 0));
            Mount.TransformBy(Matrix3d.Rotation(Math.PI/2, vRot, new Point3d(0, parameters.WidthCarcass,
                parameters.HeightCarcass / 2)));

            // Add the new object to the block table record and the transaction
            BlockTableRec.AppendEntity(Mount);
            trans.AddNewlyCreatedDBObject(Mount, true);
        }
    }
}
