﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clock3.Interface;
using Clock3.Model;
using System.Windows.Forms;
using System.IO;

using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;

namespace Clock3
{
    public class Clock : IExtensionApplication
    {
        /// <summary>
        /// Инициализация плагина
        /// </summary>
        public void Initialize()
        {
            var editor = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument.Editor;
            editor.WriteMessage("Инициализация плагина");

            GUI form = new GUI();
            form.ShowDialog();

        }
        public void Terminate()
        {
        }

        /// <summary>
        /// Эта функция будет вызываться при выполнении в AutoCAD команды "newform"
        /// </summary> 
        [CommandMethod("newformclock")]
        public void newform()
        {

            var editor = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument.Editor;
            editor.WriteMessage("Инициализация плагина");
            GUI form = new GUI();
            form.ShowDialog();
        }

        /// <summary>
        /// Нагрузочное тестирование
        /// </summary>
        [CommandMethod("startTEST")]
        public void Test()
        {   
            System.Diagnostics.Stopwatch myStopwatch = new System.Diagnostics.Stopwatch();
            StreamWriter file = new StreamWriter("D:\\Test.txt");
            for (int i = 0; i < 100; i++)
            {
                myStopwatch.Reset(); 
                myStopwatch.Start(); 

                Parameters parameters = new Parameters(ClockFigureType.Circle);
                List<double> listParam = new List<double>(){100,  10,  10,  25,  40,  10,  4,  4,  5};
                parameters.InputParameters(listParam);                
                Clock.BuildClock(parameters);

                myStopwatch.Stop();

                TimeSpan ts = myStopwatch.Elapsed;
                string elapsedTime = String.Format("{0:f}", ts.Milliseconds);
                file.Write(elapsedTime + "\n");
            }
            file.Close();
        }


        /// <summary>
        /// Построение часов
        /// </summary>        
        public static void BuildClock(Parameters parameters)
        {
            // Получаем текущий документ и его БД
            Document Doc = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;
            Database database = Doc.Database;
            // Начинаем транзакцию
            Transaction trans = database.TransactionManager.StartTransaction();

            var parts = new List<IPart>() 
            { 
                new Carcass(),
                new Cap(),
                new Mount(),
                new Pointers(),
                new Pressbutton(),
                new ClockFigures(),
                new Chain()
            };

            foreach (var part in parts)
            {
                part.CreatePart(parameters, database, trans, parameters.FigureType);
            }

            //фиксируем транзакцию 
            trans.Commit();
        }
    }
}
