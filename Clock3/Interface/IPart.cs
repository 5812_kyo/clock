﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;

namespace Clock3.Interface
{
    /// <summary>
    /// Интерфейс объектной модели часов
    /// </summary>
    interface IPart
    {
        /// <summary>
        /// Функция построения части часов
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="database"></param>
        /// <param name="trans"></param>
        /// <param name="FigureType"></param>
        void CreatePart(Parameters parameters, Database database, Transaction trans, ClockFigureType FigureType);
    }
}
