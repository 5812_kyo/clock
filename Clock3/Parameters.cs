﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clock3
{
    /// <summary>
    /// Класс, хранящий значения параметров
    /// </summary>
    public class Parameters
    {
        #region Properties

        /// <summary>
        /// Значение ширины каркаса
        /// </summary>
        public double WidthCarcass { get; private set; }

        /// <summary>
        /// Значение высоты каркаса
        /// </summary>
        public double HeightCarcass { get; private set; }

        /// <summary>
        /// Значение длины цепочки
        /// </summary>
        public double LengthChain { get; private set; }

        /// <summary>
        /// Значение ширины стрелок
        /// </summary>
        public double WidthPointers { get; private set; }

        /// <summary>
        /// Значение маленькой стрелки
        /// </summary>
        public double LengthSmallPointer { get; private set; }

        /// <summary>
        /// Значение большой стрелки
        /// </summary>
        public double LengthBigPointer { get; private set; }

        /// <summary>
        /// Значение количества отверстий на крышке
        /// </summary>
        public double NumberHoles { get; private set; }   

        /// <summary>
        /// Значение количества зубцов на кнопке
        /// </summary>
        public double NumberRotch { get; private set; }

        /// <summary>
        /// Значение длины кнопки
        /// </summary>
        public double LengthButton { get; private set; }

        /// <summary>
        /// Тип фигуры на дисплее
        /// </summary>
        public ClockFigureType FigureType { get; private set; }

        #endregion // Properties

        /// <summary>
        /// Конструктор класса Parameters
        /// </summary>
        /// <param name="figureType"></param>
        public Parameters(ClockFigureType figureType)
        {
            FigureType = figureType;
        }

        /// <summary>
        /// Проверка ввода параметров
        /// </summary>       
        public void InputParameters(List<double> parameters)
        {
            WidthCarcass = parameters[0];
            var defaultParameterRanges = GetDefaultParamterRanges();

            for (var i = 0; i < parameters.Count; i++)
            {
                if (!(defaultParameterRanges[i].Min <= parameters[i] 
                    && parameters[i] <= defaultParameterRanges[i].Max))
                {
                    throw new Exception(String.Format("Введите значение {0} от {1} до {2}", 
                        defaultParameterRanges[i].Name, defaultParameterRanges[i].Min, defaultParameterRanges[i].Max));
                }
            }

            WidthCarcass = parameters[0];
            HeightCarcass = parameters[1];
            WidthPointers = parameters[2];
            LengthSmallPointer = parameters[3];
            LengthBigPointer = parameters[4];
            LengthButton = parameters[5];
            NumberRotch = parameters[6];
            NumberHoles = parameters[7];
            LengthChain = parameters[8];
        }
        /// <summary>
        /// Диапозон минимальных и максимальных значений
        /// </summary>
        /// <returns></returns>
        private List<Parameter> GetDefaultParamterRanges()
        {
            return new List<Parameter>
            {
                new Parameter {Min = 100 , Max = 200 , Name = "Ширина каркаса", Value = 100},
                new Parameter {Min = 10, Max = 20, Name = "Высота каркаса", Value = 10 },
                new Parameter {Min =  WidthCarcass * 0.1, Max = WidthCarcass * 0.25, Name = "Ширина стрелок", Value = 10},
                new Parameter {Min =  WidthCarcass * 0.25, Max = WidthCarcass * 0.35, Name = "Длина маленькой стрелки", Value = 25},
                new Parameter {Min = WidthCarcass * 0.4, Max = WidthCarcass * 0.5, Name = "Длина большой стрелки", Value = 40},
                new Parameter {Min = 10, Max = 20, Name = "Длина кнопки", Value = 20},
                new Parameter {Min = 4, Max = 12, Name = "Количество зубцов", Value = 12},
                new Parameter {Min = 4, Max = 12, Name = "Количество отверстий на крышке", Value = 12},
                new Parameter {Min = 5, Max = 10, Name = "Длина цепочки", Value = 10}

            };
        }

     
    }

    /// <summary>
    /// Перечисление типа фигур часов
    /// </summary>
    public enum ClockFigureType
    {
        Circle = 0,
        Square,
    }

    /// <summary>
    /// Структура диапозона 
    /// </summary>
    public struct Parameter
    {
        /// <summary>
        /// Минимальное значение диапозона
        /// </summary>
        public double Min{get; set;}

        /// <summary>
        /// Максимальное значение диапозона
        /// </summary>
        public double Max { get; set; }

        /// <summary>
        /// Имя параметра
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Значение параметра
        /// </summary>
        public double Value { get; set; }
    }


}
