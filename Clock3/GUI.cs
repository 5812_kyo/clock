﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clock3
{
    public partial class GUI : Form
    {
        public GUI()
        {
            InitializeComponent();

            comboBox1.Items.Add("Окружность");
            comboBox1.Items.Add("Квадрат");
            comboBox1.SelectedIndex = 0;
        }
              

        private void WarningEmptyFields()
        {
            MessageBox.Show(this, "Заполните все поля.", "Предупреждение",
                MessageBoxButtons.OK);
        }

        private void InvalidFieldFilled()
        {
            MessageBox.Show(this, "Выберите один из вариантов дисплея", "Предупреждение",
                           MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void BuildButton(object sender, EventArgs e)
        {
             

            //Проверка на пустые поля
            if (textBox1.Text == ""
                || textBox2.Text == ""
                || textBox3.Text == ""
                || textBox4.Text == ""
                || textBox5.Text == ""
                || textBox6.Text == ""
                || textBox7.Text == ""
                || textBox9.Text == ""
                || textBox10.Text == "")
                
            {
                WarningEmptyFields();
            }
            if (comboBox1.SelectedIndex == -1)
            {
                InvalidFieldFilled();
            }


            else
            {
                List<double> parameters = new List<double>();
                parameters.Add(Convert.ToDouble(textBox1.Text));
                parameters.Add(Convert.ToDouble(textBox2.Text));
                parameters.Add(Convert.ToDouble(textBox4.Text));
                parameters.Add(Convert.ToDouble(textBox5.Text));
                parameters.Add(Convert.ToDouble(textBox6.Text));
                parameters.Add(Convert.ToDouble(textBox9.Text));
                parameters.Add(Convert.ToDouble(textBox10.Text));
                parameters.Add(Convert.ToDouble(textBox7.Text));
                parameters.Add(Convert.ToDouble(textBox3.Text));

                Parameters param = new Parameters(StringToFigureTypeConverter(comboBox1.Text));
                try
                {

                    param.InputParameters(parameters);

                    Clock.BuildClock(param);
                    Form.ActiveForm.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
        }
       
        private ClockFigureType StringToFigureTypeConverter(string figureType)
        {
            switch (figureType)
            {
                case "Окружность":
                    return ClockFigureType.Circle;
                case "Квадрат":
                    return ClockFigureType.Square;
                default:
                    throw new NotImplementedException("Этот тип не реализован!!!");

            }
        }
    }
}
